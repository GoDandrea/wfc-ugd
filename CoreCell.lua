local Pre_Process = require "PreProcess"
tile_dictionary = fetch_dictionary()
freq_rules = fetch_freq_rules()

--[[    CORE CELL   ]]-----------------------------------------------------------

CoreCell = {

    -- possible[i] == true means the tile i can be chosen for this cell
    possible = {},

    sum_of_possible_tile_weights = 0,
    --sum_of_possible_log_tile_weights = 0,

    -- tie breaker for min(entropy) calculation
    entropy_noise = 0,
    
    -- has this cell's value been decided?
    is_collapsed = false,
    
    -- tile_enabler_counts[i][d] is the count of how many tiles there could be
    -- in direction d from this cell IF this cell were the tile i
    -- d = (UP, DOWN, LEFT, RIGHT) = (1, 2, 3, 4)
    tile_enabler_counts = {}

}

function CoreCell:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.possible = {}
    for i, tileI in pairs(tile_dictionary) do
        o.possible[i] = true
    end
    o.sum_of_possible_tile_weights = o:sum_possible_frequency()
    --o.sum_of_possible_log_tile_weights = o:sum_log_frequency()
    o.entropy_noise = (math.random()/1000000)
    o:initial_tile_enabler_counts()
    o.is_collapsed = false
    return o
end


--[[ Finds the only tile i for which possible[i] is true.
    Returns i. ]]
function CoreCell:find_the_one()
    for p, tileP in pairs(self.possible) do
        --print(p, self.possible[p])
        if self.possible[p] then
            return p 
        end
    end
    -- shouldn't reach this point, since one entry in possible[] has to be true
    --print("Error at find_the_one")
    return p
end

--[[ Adds up the relative frequencies of all possible tiles.
    this corresponds to the total weight W in the simplified
    entropy equation ]]
function CoreCell:sum_possible_frequency()
    local total = 0
    for i, tileI in pairs(tile_dictionary) do  
        if self.possible[i] then
            total = total + freq_rules[i]
        end
    end
    return total
end

--[[ Similar to sum_possible_frequency, but for each frequency F
    it will add F * log(F) ]]
--[[
function CoreCell:sum_log_frequency()
    local total = 0
    local aux = 0
    for i, tileI in pairs(tile_dictionary) do  
        if self.possible[i] and freq_rules[i] > 0 then
            aux = freq_rules[i] * math.log(freq_rules[i])
            total = total + aux
        end
    end
    return total
end
]]

function CoreCell:remove_tile (tile_index)
    if self.possible[tile_index] then
        self.possible[tile_index] = false
        local freq = freq_rules[tile_index]
        self.sum_of_possible_tile_weights = self.sum_of_possible_tile_weights - freq 
    end
    --self.sum_of_possible_log_tile_weights = self.sum_of_possible_log_tile_weights - (freq * math.log(freq))

end

function CoreCell:entropy ()
    --local aux = math.log(self.sum_of_possible_tile_weights)
    --if aux < 0 then aux = 0 end
    --return (aux - (self.sum_of_possible_log_tile_weights / self.sum_of_possible_tile_weights) + self.entropy_noise)
    return self.sum_of_possible_tile_weights
end

--[[
function CoreCell:print_entropy () --for debugging
    print(self.sum_of_possible_tile_weights, self.sum_of_possible_log_tile_weights, self.entropy_noise)
    return math.log(self.sum_of_possible_tile_weights) 
    - (self.sum_of_possible_log_tile_weights / self.sum_of_possible_tile_weights)
    + self.entropy_noise
end
]]

-- Chooses which tile will be put here
function CoreCell:choose_tile_index ()
    local remaining = (math.random(self.sum_of_possible_tile_weights + 1) - 1)
    local weight
    for i, cell in pairs(self.possible) do
        if self.possible[i] then 
            weight = freq_rules[i]
            if remaining > weight then
                --print(i, remaining, weight)
                remaining = remaining - weight
            else
                return i
            end
        end
    end
    print("Error at choose_tile_index")
    for i, cellI in pairs(self.possible) do
        --print(i, self.possible[i])
        if self.possible[i] then
            return i
        end
    end
end

-- Initializes tile_enabler_counts
function CoreCell:initial_tile_enabler_counts ()

    self.tile_enabler_counts = {}

    for i, tileI in pairs(tile_dictionary) do
        self.tile_enabler_counts[i] = {}
        for d=1,4 do
            self.tile_enabler_counts[i][d] = 0
            for j, tileJ in pairs(tile_dictionary) do
                if adj_rules[i][d][j] then self.tile_enabler_counts[i][d] = self.tile_enabler_counts[i][d] + 1 end
            end
        end
    end
end

-- Checks if a tile has enabler_counts[tile][d] = 0 for some direction
function CoreCell:has_zero_count (tile)
    
    for d=1,4 do
        if self.tile_enabler_counts[tile][d] == 0 then return true end
    end
    
    return false
end

function CoreCell:has_possible_tiles ()
    local possible = {}
    for d=1,4 do
        possible[d] = false
        for i, tileI in pairs(tile_dictionary) do
            if self.tile_enabler_counts[i][d] ~= 0 then possible[d] = true end
        end
    end
    
    return (possible[1] and possible[2] and possible[3] and possible[4])
end