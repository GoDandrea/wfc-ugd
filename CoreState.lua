local Core_Cell = require "CoreCell"
local Entropy = require "Entropy"

local stack = require "stack"
local heap = require "binaryheap"

--[[    CORE STATE  ]]-----------------------------------------------------------

CoreState = {

    -- 2D array of CoreCells
    grid = {},

    remaining_cells = 0,

    -- binary heap of EntropyCoords
    entropy_heap = heap.minHeap(compare),

     -- stack of RemovalUpdates
    tile_removals = Stack:new()

}

RemovalUpdate = {
    tile_index = nil,
    x = nil,
    y = nil
}

function RemovalUpdate:new (i, x, y, o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.tile_index = i
    o.x = x
    o.y = y
    return o
end

function RemovalUpdate:neighbor (direction)
    -- direction (1,2,3,4) = (UP,DOWN,LEFT,RIGHT)
    coord = {
        x = self.x,
        y = self.y
    }
    neighbor = {
        [1] = function() coord.y = self.y - 1 end,
        [2] = function() coord.y = self.y + 1 end,
        [3] = function() coord.x = self.x - 1 end,
        [4] = function() coord.x = self.x + 1 end
    }
    do neighbor[direction]() end
    return coord
end


function CoreState:new (o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    self.remaining_cells = grid.x * grid.y
    local aux = 0
    for i=0,grid.y-1 do
        for j=0,grid.x-1 do
            aux = grid[i][j]:entropy()
            local entropy_init = EntropyCoord:new(j,i,aux)
            self.entropy_heap:insert(entropy_init)
            --print("Inserting", entropy_init.entropy, entropy_init.x, entropy_init.y)
        end
    end
    return o
end


-- Returns the coords of the next cell to collapse
function CoreState:choose_next_cell ()

    local top = self.entropy_heap:pop()
    --print ("Popping:", top.entropy, top.x, top.y)
    while top do
        local cell = grid[top.y][top.x]
        if not cell.is_collapsed then
            return top
        end
        top = self.entropy_heap:pop()
    end
    print("Error: entropy_heap is empty, but there are still uncollapsed cells.")
end


-- Collapses the cell at the given coord
function CoreState:collapse_cell_at (coord)
    local cell = grid[coord.y][coord.x]
    local tile_index_to_lock = cell:choose_tile_index()
    cell.is_collapsed = true
    self.remaining_cells = self.remaining_cells - 1
    for i, valueI in pairs(cell.possible) do
        if (i ~= tile_index_to_lock) and cell.possible[i] then
            cell.possible[i] = false
            -- print(coord.x, coord.y, cell.possible[i], i)
            r_update = RemovalUpdate:new(i, coord.x, coord.y)
            self.tile_removals:push(r_update)
            --print("Pushing", r_update.tile_index)
        end
    end
    --local test = self.tile_removals:pop()
    --while test do
    --    print(test.x, test.y, test.tile_index)
    --    test = self.tile_removals:pop()
    --end
end

-- Returns the direction opposite to direct
function opposite(direct)
    if direct == 1 then
        return 2
    end
    if direct == 2 then
        return 1
    end
    if direct == 3 then
        return 4
    end
    if direct == 4 then
        return 3
    end
end

-- Remove possibilities based on collapsed cell
function CoreState:propagate ()

    -- At some point in the recent past, r_update.tile_index was
    -- removed as a candidate for the tile in the cell at r_update coords.
    local r_update = self.tile_removals:pop()
    
    while r_update and r_update.tile_index do
        --print("Propg" , r_update.x, r_update.y)

        -- Propagate the effect to the neighbors in each direction
        for d=1,4 do
            local neighbor_coord = r_update:neighbor(d)
            if not (neighbor_coord.y > grid.y-1 or neighbor_coord.y < 0 or
                    neighbor_coord.x > grid.x-1 or neighbor_coord.x < 0) then
                local neighbor_cell = {}
                neighbor_cell = grid[neighbor_coord.y][neighbor_coord.x]
                -- Iterate over all tiles that may appear in direction [d] from
                -- a cell containing r_update.tile_index
                for i, tileI in pairs(tile_dictionary) do
                    if adj_rules[r_update.tile_index][d][i] then
                        -- Relative to neighbour_cell, the cell at the r_update coords
                        -- is in the opposite direction to [d]
                        local op_d = opposite(d)
                        local enabler_counts = neighbor_cell.tile_enabler_counts[i][op_d]
                        -- Check if we're going to reduce this to 0
                        --print("Checking tile", i, "at", neighbor_coord.x, neighbor_coord.y)
                        --print("enabler_counts", enabler_counts)
                        if enabler_counts == 1 then
                            -- If there's a zero count in another direction, the potential
                            -- tile has already been removed, and we won't remove it again.
                            if not neighbor_cell:has_zero_count(i) and neighbor_cell.possible[i] then
                                neighbor_cell:remove_tile(i)
                                if neighbor_cell:has_possible_tiles() == false then
                                    print("Error: contradiction")
                                end
                                -- This probably changed the cell's entropy.
                                local entropy_update = {}
                                local new_etropy = neighbor_cell:entropy()
                                entropy_update = EntropyCoord:new(neighbor_coord.x, neighbor_coord.y, new_etropy)
                                self.entropy_heap:insert(entropy_update)
                                -- Add the update to the stack
                                
                                
                                local new_update = {}
                                new_update = RemovalUpdate:new()
                                new_update.tile_index = i
                                new_update.x = neighbor_coord.x
                                new_update.y = neighbor_coord.y
                                self.tile_removals:push(new_update)
                                
                                
                            end
                        end
    
                        neighbor_cell.tile_enabler_counts[i][op_d] = enabler_counts - 1

                    end
                end
            end
        end
        r_update = self.tile_removals:pop()
    end
end


function CoreState:run ()
    local next_coord
    while self.remaining_cells > 0 do
        --print("Remaining cells:", self.remaining_cells)
        next_coord = self:choose_next_cell()
        self:collapse_cell_at(next_coord)
        self:propagate()
    end
end
