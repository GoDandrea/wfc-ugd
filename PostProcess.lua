local Pre_Process = require "PreProcess"

function wfc_postprocess(out_w, out_h, scale, grid)
    
    love.graphics.scale(scale,scale)

    for x=0, out_w-1 do
        for y=0, out_h-1 do
            local R,G,B = find_RGB(grid[y][x])
            love.graphics.setColor(R,G,B)
            love.graphics.rectangle("fill",x,y,1,1)
        end
    end

end